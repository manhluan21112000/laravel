<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=> [
                'required',
                'string',
                'max:255'
            ],
            'address' => [
                'required',
                'string',
                'max:255'
            ],
            'avatar' => [
                'image',
                'max:2048',
            ],
            'description' => [
                'max:500'
            ],
            'birthday' => [
                'required',
                // 'date',
            ],
        ];
    }
}
