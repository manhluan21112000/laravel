<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory;

    protected $table = 'user';
    public $timestamps = true;
    // use SoftDeletes, HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'address',
        'avatar',
        'birthday',
        'description',
    ];

    public $sortable = ['code', 'name', 'created_at'];

}
