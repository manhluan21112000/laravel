@extends('layout.master')
@section('content')
    <form action="{{ route('store') }}" method="post" id="form-add" enctype="multipart/form-data">
        @csrf
        <div style="display: flex; justify-content: space-between">
            <a href="{{ route('index') }}" class="btn btn-primary">Back</a>
            <button class="btn btn-danger" type="reset" id="reset">Clear</button>
        </div>
        <br>
        <div>
            <label class="form-lable">Name <span style="color: red">(*)</span></label>
            <input type="text" placeholder="Name" name="name" id="title-add" class="form-control">
            @error('name')
                <span style="color: red;">{{ $message }}</span>
            @enderror
        </div>
        <br>
        <div>
            <label class="form-lable">Address <span style="color: red">(*)</span></label>
            <input class="form-control" type="text" placeholder="address" name="address">
            @error('address')
                <span style="color: red;">{{ $message }}</span>
            @enderror
        </div>
        <br>
        <div>
            <label class="form-lable">Image</label>
            <input class="form-control" type="file" placeholder="avatar" name="avatar">
        </div>
        <br>
        <div>
            <label class="form-lable">Birthday <span style="color: red">(*)</span></label>
            <input class="form-control" type="date" placeholder="birthday" name="birthday">
            @error('birthday')
                <span style="color: red;">{{ $message }}</span>
            @enderror
        </div>
        <br>
        <div>
            <label class="form-lable">Description</label>
            <textarea class="form-control" name="description" id="description"></textarea>
        </div>
        <br>
        <button type="submit" class="btn btn-primary" id="button-insert">Them moi</button>
    </form>
@endsection
