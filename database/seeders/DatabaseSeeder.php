<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DB::table('user')->insert([
            'name'        => 'luan',
            'address'     => 'ha noi',
            'avatar' => null,
            'birthday'    => '2000-11-21',
            'description' => 'ssssss'
        ]);
        // User::create([
        //     'name'        => 'luan',
        //     'address'     => 'ha noi',
        //     'birthday'    => '22',
        //     'description' => 'ssssss'
        // ]);


        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
