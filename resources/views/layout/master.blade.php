<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}" type="text/css">
    <title>Document</title>
</head>
<body>
    <div class="container mt-3">
        <img src="https://pbs.twimg.com/profile_images/145133825/pirago_400x400.jpg" alt="anh mang" style="width: 100px; height: 100px">
    </div>
    <div class="container mt-3">
        <h4 class="text-dark" style="opacity: 0.5;">{{ $title ?? '' }}</h4>
        @yield('content')
    </div>
</body>
</html>