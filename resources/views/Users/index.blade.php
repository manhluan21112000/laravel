@extends('layout.master')
@section('content')
    <div>
        <div>
            <a href="{{ route('create') }}" class="btn btn-primary">Add User</a>
        </div>
        <br>
    </div>
    <div id="data">
        @include('Users.item')
    </div>
@endsection

