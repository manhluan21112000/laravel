<table border="1" style="width: 100%" class="table table-striped">
    <thead>
        <tr>
            <th style="text-align: center">Name</th>
            <th style="text-align: center">Image</th>
            <th style="text-align: center">Birthday</th>
            <th style="text-align: center">Address</th>
        </tr>
    </thead>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <td style="text-align: center; vertical-align: middle"><a
                        href="{{ route('detail', $user->id) }}">{{ $user->name }}</a></td>
                <td style="width: 100px; height:100px"><img
                        style="width: 100%; height: 100%; border-radius: 10px"
                        src="images/user/{{ isset($user->avatar) ? $user->avatar : 'anh_default.png' }}"
                        alt=""></td>
                <td style="text-align: center; vertical-align: middle">
                    {{ Carbon\Carbon::parse($user->birthday)->format('Y-m-d') }}</td>
                <td style="text-align: center; vertical-align: middle">{{ $user->address }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<br>
    <div style="float:right;">
        {{ $users->links('pagination.custom') }}
    </div>

