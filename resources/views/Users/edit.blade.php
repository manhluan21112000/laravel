@extends('layout.master')
@section('content')
    <form action="{{ route('update', $editUser->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div>
            <a href="{{ route('detail', $editUser->id) }}" class="btn btn-primary">Back</a>
        </div>
        <br>
        <div>
            <label  class="form-lable">Name <span style="color: red">(*)</span></label>
            <input type="text" placeholder="Name" name="name" class="form-control" value="{{ $editUser->name }}">
        </div>
        <br>
        <div>
            <label  class="form-lable">Address <span style="color: red">(*)</span></label>
            <input class="form-control" type="text" placeholder="address" name="address" value="{{ $editUser->address }}">
        </div>
        <br>
        <div>
            <label class="form-lable">Image</label> <br>
            <img class="rounded-circle shadow-4" style="width: 100px;" src="/images/user/{{ isset($editUser->avatar) ? $editUser->avatar : 'anh_default.png' }}" alt="avatar">
            <br> 
            <input class="form-control" type="file" placeholder="avatar" name="avatar" >
        </div>
        <br>
        <div>
            <label class="form-lable">Birthday <span style="color: red">(*)</span></label>
            <input class="form-control" type="date" name="birthday" value="{{ Carbon\Carbon::parse($editUser->birthday)->format('d-m-Y') }}">
        </div>
        <br>
        <div>
            <label class="form-lable">Description</label>
            <textarea class="form-control" name="description" id="description">{{ isset($editUser->description) ? $editUser->description : '' }}</textarea>
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Them moi</button>
    </form>
@endsection