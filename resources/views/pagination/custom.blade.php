@if ($paginator->hasPages())
<ul class="pager" >
    @if ($paginator->onFirstPage())
        <a class="disabled" style="text-decoration: none; color: black "><span >← Previous</span></a>
    @else
        <a href="{{ $paginator->previousPageUrl() }}" rel="prev" style="text-decoration: none; color: black">← Previous</a>
    @endif
    @foreach ($elements as $element)
        @if (is_string($element))
            <a class="disabled" style="text-decoration: none; color: black"><span>{{ $element }}</span></a>
        @endif
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <a class="active my-active " style="text-decoration: none; padding:8px 16px; border-style: groove;  color: black; background-color: #827e7e "><span>{{ $page }}</span></a>
                @else
                    <a href="{{ $url }}" style="text-decoration: none; padding:8px 16px; color: black ">{{ $page }}</a>
                @endif
            @endforeach
        @endif
    @endforeach
    @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" style="text-decoration: none; color: black " rel="next">Next →</a>
    @else
        <a class="disabled" style="text-decoration: none; color: black "><span>Next →</span></a>
    @endif
</ul>
@endif 