<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\StoreRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\UpdateRequest;
use Illuminate\Support\Facades\View;


class UserController extends Controller
{
    // use ResponseTrait;

    function __construct()
    {
        $routeName = Route::currentRouteName();
        $arr = explode('.', $routeName);
        $arr = array_map('ucfirst', $arr);
        $title = implode(' - ', $arr);
        View::share('title', $title);
    }

    
    public function index(Request $request)
    {
        $users = User::orderByDesc('id')->paginate(2);

        if ($request->ajax()) {
            return view('Users.index', [
                'users' => $users,
            ]);
        }

        return view('Users.index', [
            'users' => $users,
        ]);
    }

    public function create()
    {
        return view('Users.create');
    }

    public function store(StoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->validated();

            if ($request->hasFile('avatar')) {
                $imageName = uniqid() . time() . '.' . $request->avatar->extension();
                $request->avatar->move(public_path('images/user'), $imageName);
                $data['avatar'] = $imageName;
            }
            // dd($data);
            User::create($data);
            DB::commit();
            return redirect()->route('index')->with('success','create user success');
        } catch (\Exception $e) {
            // dd($e);
            // \Illuminate\Support\Facades\Log::error($e);
            DB::rollBack();

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('Users.detail', [
            'users' => $user,
        ]);
    }

    public function update(UpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $object = User::find($id);
            $object->fill($request->validated());

            if ($request->hasFile('avatar')) {
                $imageName = uniqid() . time() . '.' . $request->avatar->extension();
                $request->avatar->move(public_path('images/user'), $imageName);
                $object['avatar'] = $imageName;
            }
            $object->save();
            DB::commit();

            return redirect()->route('index')->with('success', 'Cập nhật thành công');
        } catch (\Throwable $e) {
            dd($e);
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function edit($id)
    {
        $editUser = User::find($id);
        return view('Users.edit', ['editUser' => $editUser]);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('index');
    }
}
